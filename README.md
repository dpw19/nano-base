# Base para Arduino Nano

Esta base facilita el trabajo con Arduino Nano en tus proyectos ya que es de muy bajo costo y permite hacer las conexiones de tu proyecto y tener la posibilidad de mover tu Arduino a otro proyecto sin tener que desconectar tus drivers, sensores u actuadores.

Su manufactura es de una sola capa de pistas lo que facilita su producción con métodos caseros.

Cuenta con expansión para las conexiones de Vin, GND, 5V y 3V3.

![Imagen 3D base arduino nano, vista ffront](img/nano-base_F.png)

![Imagen 3D base arduino nano, vista bottom](img/nano-base_B.png)

En el directorio [kicad](kicad) se encuentra el proyecto nano-base por si deseas hacer algún ajuste; así como los archivos del esquemático y el PCB.

Si lo quieres pasar al cobre tal como está puedes encontrar los PDF en el directorio [img](img) separados por capas.
