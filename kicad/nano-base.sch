EESchema Schematic File Version 4
LIBS:nano-base-cache
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Base para Arduino Nano"
Date "2019-10-08"
Rev "01"
Comp "DPW19"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x15_Female J3
U 1 1 5D42451D
P 5000 3700
F 0 "J3" H 5028 3726 50  0000 L CNN
F 1 "Nano" H 5028 3635 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 5000 3700 50  0001 C CNN
F 3 "~" H 5000 3700 50  0001 C CNN
	1    5000 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x15_Female J1
U 1 1 5D42523F
P 4450 3700
F 0 "J1" H 4342 4585 50  0000 C CNN
F 1 "Nano" H 4342 4494 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 4450 3700 50  0001 C CNN
F 3 "~" H 4450 3700 50  0001 C CNN
	1    4450 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4650 3000 4800 3000
Wire Wire Line
	4650 3100 4800 3100
Wire Wire Line
	4650 3200 4800 3200
Wire Wire Line
	4650 3300 4800 3300
Wire Wire Line
	4650 3400 4800 3400
Wire Wire Line
	4650 3500 4800 3500
Wire Wire Line
	4650 3600 4800 3600
Wire Wire Line
	4650 3700 4800 3700
Wire Wire Line
	4650 3800 4800 3800
Wire Wire Line
	4650 3900 4800 3900
Wire Wire Line
	4650 4000 4800 4000
Wire Wire Line
	4650 4100 4800 4100
Wire Wire Line
	4650 4200 4800 4200
Wire Wire Line
	4650 4300 4800 4300
Wire Wire Line
	4650 4400 4800 4400
$Comp
L Connector:Conn_01x15_Female J6
U 1 1 5D444B53
P 6550 3700
F 0 "J6" H 6578 3726 50  0000 L CNN
F 1 "Nano" H 6578 3635 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 6550 3700 50  0001 C CNN
F 3 "~" H 6550 3700 50  0001 C CNN
	1    6550 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x15_Female J5
U 1 1 5D444B59
P 6000 3700
F 0 "J5" H 5892 4585 50  0000 C CNN
F 1 "Nano" H 5892 4494 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 6000 3700 50  0001 C CNN
F 3 "~" H 6000 3700 50  0001 C CNN
	1    6000 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 3000 6350 3000
Wire Wire Line
	6200 3100 6350 3100
Wire Wire Line
	6200 3200 6350 3200
Wire Wire Line
	6200 3300 6350 3300
Wire Wire Line
	6200 3400 6350 3400
Wire Wire Line
	6200 3500 6350 3500
Wire Wire Line
	6200 3600 6350 3600
Wire Wire Line
	6200 3700 6350 3700
Wire Wire Line
	6200 3800 6350 3800
Wire Wire Line
	6200 3900 6350 3900
Wire Wire Line
	6200 4000 6350 4000
Wire Wire Line
	6200 4100 6350 4100
Wire Wire Line
	6200 4200 6350 4200
Wire Wire Line
	6200 4300 6350 4300
Wire Wire Line
	6200 4400 6350 4400
Text Label 4650 4400 0    50   ~ 0
Vin
Text Label 4650 4300 0    50   ~ 0
GND
Text Label 4650 4100 0    50   ~ 0
5V
Text Label 4650 3100 0    50   ~ 0
3V3
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5D44C1AD
P 4550 4900
F 0 "J2" H 4442 5185 50  0000 C CNN
F 1 "Supply" H 4442 5094 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 4550 4900 50  0001 C CNN
F 3 "~" H 4550 4900 50  0001 C CNN
	1    4550 4900
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5D44FCCE
P 5350 4900
F 0 "J4" H 5378 4876 50  0000 L CNN
F 1 "Supply" H 5378 4785 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 5350 4900 50  0001 C CNN
F 3 "~" H 5350 4900 50  0001 C CNN
	1    5350 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4800 5150 4800
Wire Wire Line
	4750 5000 5150 5000
Wire Wire Line
	5150 5100 4750 5100
Text Label 4800 5100 0    50   ~ 0
Vin
Text Label 4800 5000 0    50   ~ 0
GND
Wire Wire Line
	4750 4900 5150 4900
Text Label 4800 4900 0    50   ~ 0
5V
Text Label 4800 4800 0    50   ~ 0
3V3
Text Label 6200 4100 0    50   ~ 0
GND
$EndSCHEMATC
